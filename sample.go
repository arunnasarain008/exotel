package main

import "fmt"

func leapYear(year int) bool {

	if year%4 == 0 && year%100 != 0 {
		return true
	} else if year%100 == 0 && year%400 == 0 {
		return true
	}
	return false
}

func month(month int, year int) int {

	if month == 4 || month == 6 || month == 9 || month == 11 {
		return 30
	} else if month == 2 {
		if !leapYear(year) {
			return 28
		}
		return 29
	} else {
		return 31
	}
}

func main() {
	//date1 := [3]int{27, 10, 2020}
	//date2 := [3]int{21, 5, 2035}

	diff := 0
	var date1 [3]int
	var date2 [3]int

	fmt.Println("Start date:")
	fmt.Scanln(&date1[0])
	fmt.Scanln(&date1[1])
	fmt.Scanln(&date1[2])

	fmt.Println("End date:")
	fmt.Scanln(&date2[0])
	fmt.Scanln(&date2[1])
	fmt.Scanln(&date2[2])

	///////////////////////////////////////

	// Adding the no. of days in each year between two dates
	if date1[2] != date2[2] {
		for i := date1[2]; i < date2[2]; i++ {
			if leapYear(i) {
				diff += 366
			} else {
				diff += 365
			}
		}
	}

	// Subtracting the additional months if the month of start date is higher
	if date1[1] > date2[1] {
		for i := date2[1]; i < date1[1]; i++ {
			days := month(i, date2[2])
			diff -= days
		}
		diff += month(date1[1], date1[2])

		// Adding the additional dates if month of end date is higher
	} else if date1[1] < date2[1] {
		for i := date1[1]; i <= date2[1]; i++ {
			days := month(i, date1[2])
			diff += days
		}
	} else {
		diff += month(date1[1], date1[2])
	}

	diff -= date1[0]
	total := month(date2[1], date2[2])
	diff -= (total - date2[0])

	fmt.Println(diff)
}
